require 'rails_helper'

RSpec.feature "CategoriesShow", type: :feature do
  feature "shows page accurately" do
    given(:language)        { create(:taxonomy, name: "Language") }
    given(:two_products)    { create_list(:product, 2) }
    given(:three_products)  { create_list(:product, 3) }
    given(:java) do
      create(:taxon, name: "Java", taxonomy: language, products: three_products)
    end
    given(:ruby) do
      create(:taxon, name: "Ruby", taxonomy: language)
    end
    given(:rails) do
      create(:taxon, name: "Rails", taxonomy: language, products: two_products)
    end

    background do
      java.move_to_child_of(java.taxonomy.root)
      ruby.move_to_child_of(ruby.taxonomy.root)
      rails.move_to_child_of ruby
    end

    feature "sidebar and related_products", js: true do
      scenario "check sidebar contents & links" do
        visit potepan_category_path(rails.id)
        expect(current_path).to eq potepan_category_path(rails.id)
        expect(page).to have_content "Language"
        expect(page).not_to have_content "Rails"
        find("ul.navbar-nav", text: "Language").click
        expect(page).to have_no_selector 'ul.collapse', text: "Ruby"
        expect(page).to have_selector 'ul.collapse', text: "Rails"
        find("ul.navbar-nav", text: "Java").click
        expect(current_path).to eq potepan_category_path(java.id)
        expect(page).to have_title "Java"
        expect(page).to have_selector "div.productImage", count: 3
        expect(page).to have_link java.products[0].name
        expect(page).to have_content java.products[0].price
      end

      scenario "show products and link to detail page" do
        visit potepan_category_path(rails.id)
        expect(page).to have_title "Rails"
        expect(page).to have_selector "div.productImage", count: 2
        expect(page).to have_link rails.products[0].name
        expect(page).to have_content rails.products[0].price
        find_link(rails.products[0].name).click
        expect(current_path).to eq potepan_product_path(rails.products[0].id)
      end
    end
  end
end
