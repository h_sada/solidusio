require 'rails_helper'

RSpec.feature "ProductsShow", type: :feature do
  feature "shows page accurately" do
    given!(:taxon) { create(:taxon, name: "Brand") }
    given!(:products) { create_list(:product, 5, taxon_ids: taxon.id) }
    given(:images) { create_list(:image, 3) }
    given!(:child) { create(:taxon, name: "Ruby", parent_id: taxon.id) }
    given!(:product) { create(:product, name: "Ruby-Ring", taxon_ids: child.id) }

    background do
      products[0].images << images
    end

    context "main_products" do
      scenario "shows name, price, description" do
        visit potepan_product_path(products[0].id)
        expect(page).to have_title products[0].name
        expect(page).to have_selector "h2", text: products[0].name
        expect(page).to have_selector "h3", text: products[0].price
        expect(page).to have_selector "p", text: products[0].description
      end
      scenario "shows images on carousel" do
        visit potepan_product_path(products[0].id)
        expect(page).to have_css("img[src*='/large/']", count: 3)
        expect(page).to have_css("img[src*='/small/']", count: 3)
      end
      scenario "shows all images on carousel" do
        visit potepan_product_path(products[0].id)
        expect(page).to have_selector ".item", count: products[0].images.count
        expect(page).to have_selector ".thumb", count: products[0].images.count
      end
    end

    context "related images" do
      scenario "shows name, price, description" do
        visit potepan_product_path(products[0].id)
        expect(page).to have_css "img[src*='/product/']"
        expect(page).to have_selector ".productCaption", text: taxon.products[0].name
        expect(page).to have_selector ".productCaption", text: taxon.products[0].price
      end
      scenario "shows related_products 4pcs, not 5pcs" do
        visit potepan_product_path(products[0].id)
        expect(page).to have_selector ".productBox", count: 4
        expect(page).not_to have_selector ".productBox", count: 5
      end
      scenario "links to category_page" do
        visit potepan_product_path(products[0].id)
        find("h5", text: taxon.products[0].name).click
        expect(current_path).to eq potepan_product_path(taxon.products[0].id)
      end
    end

    context "back to prior page" do
      context "prior page exists" do
        scenario "prior page is category page" do
          visit potepan_category_path(child.id)
          expect(current_path).to eq potepan_category_path(child.id)
          click_on "Ruby-Ring"
          expect(current_path).to eq potepan_product_path(product.id)
          click_on "一覧ページへ戻る"
          expect(current_path).to eq potepan_category_path(child.id)
        end
        scenario "prior page is NOT category_page" do
          visit potepan_product_path(product.id)
          click_on "Ruby-Ring"
          click_on "一覧ページへ戻る"
          expect(current_path).to eq potepan_category_path(taxon.id)
        end
      end

      context "prior page NOT exists" do
        scenario "links to product_page directly" do
          visit potepan_product_path(product.id)
          expect(current_path).to eq potepan_product_path(product.id)
          click_on "一覧ページへ戻る"
          expect(current_path).to eq potepan_category_path(taxon.id)
        end
      end
    end
  end
end
