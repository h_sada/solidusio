require 'rails_helper'

RSpec.describe "ProductsResponses", type: :request do
  let(:taxon)    { create(:taxon) }
  let(:products) { create_list(:product, 4, taxon_ids: taxon.id) }
  let(:related_products) do
    taxon.products = []
    taxon.products.push products
  end

  describe "get #show" do
    it "returns status success" do
      get potepan_product_path(products[0].id)
      expect(response).to be_successful
    end

    it "returns status 200" do
      get potepan_product_path(products[0].id)
      expect(response).to have_http_status 200
    end

    it "gets show_template" do
      get potepan_product_path(products[0].id)
      expect(response).to render_template :show
    end

    it "gets @product" do
      get potepan_product_path(products[0].id)
      expect(assigns(:product)).to eq products[0]
    end

    it "gets @related_products" do
      get potepan_product_path(products[0].id)
      expect(related_products).not_to be_empty
      expect(assigns(:related_products)).to match related_products
      expect(assigns(:related_products).count).to eq 4
    end
  end

  describe "can't get #show" do
    it "returns RecordNotFound" do
      expect { get potepan_product_path(-1) }.to \
        raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
