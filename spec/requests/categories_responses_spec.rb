require 'rails_helper'

RSpec.describe "CategoriesResponses", type: :request do
  let(:language)       { create(:taxonomy, name: "Language") }
  let(:rails_products) { create_list(:product, 5) }
  let(:java_products)  { create_list(:product, 5) }
  let(:rails) do
    create(:taxon, taxonomy: language, name: "Rails", products: rails_products)
  end
  let(:java) do
    create(:taxon, taxonomy: language, name: "Java", products: java_products)
  end

  before do
    rails.move_to_child_of(rails.taxonomy.root)
    java.move_to_child_of(java.taxonomy.root)
  end

  describe "gets#show" do
    it "gets status 200" do
      get potepan_category_path(rails.id)
      expect(response).to have_http_status 200
    end

    it "gets status success" do
      get potepan_category_path(rails.id)
      expect(response).to be_successful
    end

    it "gets #show" do
      get potepan_category_path(rails.id)
      expect(response).to render_template :show
    end

    it "gets @taxon" do
      get potepan_category_path(rails.id)
      expect(assigns(:taxon)).to eq rails
      expect(assigns(:taxon)).not_to eq java
    end

    it "gets @products" do
      get potepan_category_path(rails.id)
      expect(assigns(:products)).not_to be_empty
      expect(assigns(:products)).to eq rails.products
      expect(assigns(:products)).not_to eq java.products
    end

    it "gets @taxonomies" do
      get potepan_category_path(rails.id)
      expect(assigns(:taxonomies)).to contain_exactly language
    end
  end

  describe "can't get #show" do
    it "returns RecordNotFound" do
      expect { get potepan_category_path(-1) }.to \
        raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
