class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :store_user_location!, if: :storable_location?

  def prms
    { color: params[:color], size: params[:size] }
  end

  def prm_value(param_name)
    params[prm_key(param_name)]
  end

  def prm_key(param_name)
    if param_name.is_a?(Symbol)
      param_name
    elsif param_name.is_a?(String)
      :"#{param_name.downcase}"
    end
  end

  def param_nil?(param_sym)
    prms[prm_key(param_sym)].nil?
  end

  private

  def storable_location?
    request.get? && is_navigational_format? && !devise_controller? && !request.xhr?
  end

  def store_user_location!
    store_location_for(:potepan_user, request.fullpath)
  end

  def after_sign_out_path_for(resource_or_scope)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    router_name = Devise.mappings[scope].router_name
    context = router_name ? send(router_name) : self
    context.respond_to?(:root_path) ? context.root_path : "/potepan"
  end
end
