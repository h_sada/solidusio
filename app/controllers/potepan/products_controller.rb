class Potepan::ProductsController < ApplicationController
  # before_action :authenticate_potepan_user!
  RELATED_PRODUCTS_NUMBERS = 4

  def show
    @product = select_products(params[:id])
    @variant = Spree::Variant.find_by_color(params[:color])
    @main_images = select_images(product: @product, variant: @variant)
    @related_products = Spree::Product.find_related_products(@product).
      limit(RELATED_PRODUCTS_NUMBERS)
    @prior_url = prior_url(@product)
  end

  def select_images(args)
    if params[:color].nil?
      default_images(args)
    else
      color_images(args)
    end
  end

  def select_products(param)
    if params[:color].nil?
      product(param)
    else
      variant_products(param)
    end
  end

  private

  def product(param)
    Spree::Product.find(param)
  end

  def variant_products(param)
    Spree::Product.joins_option_values(param)
  end

  def color_images(args)
    args[:product].variant_images.find_color_images(args)
  end

  def default_images(args)
    args[:product].images
  end

  def prior_url(product)
    if request.referrer.nil? || !request.referrer.include?("/categories/")
      potepan_category_path(product.taxons.first.root.id)
    else
      request.referrer
    end
  end
end
