class Potepan::HomeController < ApplicationController
  BAGS_CATEGORY_ID = 3
  MUGS_CATEGORY_ID = 4
  SHIRTS_CATEGORY_ID = 6

  def index
    category_ids = [SHIRTS_CATEGORY_ID, BAGS_CATEGORY_ID, MUGS_CATEGORY_ID]
    @categories = Spree::Taxon.arrange_hot_categories_by(category_ids)
    @latest_products = Spree::Product.includes_master.order(available_on: :desc)
  end
end
