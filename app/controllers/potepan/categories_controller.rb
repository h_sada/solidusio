class Potepan::CategoriesController < ApplicationController
  before_action :prms, only: :show
  before_action :define_params_names, only: :show
  before_action :select_active_btn, only: :show
  helper_method :prm_value, :prm_key, :other_prm

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(:root)
    @option_types = Spree::OptionType.includes(:option_values).all
    @products_or_variants = products_or_variants(param: @params_name, taxon: @taxon)
    @grid_params = link_params("grid")
    @list_params = link_params("list")
  end

  def define_params_names
    @params_name = params_name
    @another_params_name = another_param_name
  end

  def params_name
    prm_key judge_param_name(param_sym: :size, another_param_sym: :color)
  end

  def another_param_name
    prm_key judge_param_name(param_sym: :color, another_param_sym: :size)
  end

  private

  def link_params(type)
    hash = {}
    active_params(hash: hash, key: :source, val: type)
    active_params(hash: hash, key: @params_name, val: prm_value(@params_name))
    active_params(hash: hash, key: @another_params_name, val: prm_value(@another_params_name))
    hash
  end

  def active_params(args)
    args[:hash].store(args[:key], args[:val].to_sym) unless args[:val].nil?
  end

  def select_active_btn
    @grid = @list = nil
    if params[:source] == "grid" || params[:source].nil?
      @grid = "active"
    elsif params[:source] == "list"
      @list = "active"
    end
  end

  def other_prm(args)
    args[:variant].option_values.where.not(presentation: args[:prm_value]).first.presentation
  end

  def judge_param_name(args)
    if param_nil?(:color) && param_nil?(:size)
      nil
    elsif param_nil?(:color)
      args[:param_sym]
    elsif param_nil?(:size)
      args[:another_param_sym]
    end
  end

  def products_or_variants(args)
    if param_nil?(args[:param])
      select_taxon_products(args)
    else
      select_variants(args[:param])
    end
  end

  def select_taxon_products(args)
    Spree::Product.find_taxon_products(args)
  end

  def select_variants(prm_value)
    Spree::Variant.joins(:option_values).
      where("spree_option_values.presentation":
      :"#{prm_value(prm_value)}").includes(:product)
  end
end
