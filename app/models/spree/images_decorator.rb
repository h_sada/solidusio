Spree::Image.class_eval do
  scope :find_color_images, lambda { |args|
    select { |images| images.viewable_id == args[:variant].id }
  }
end
