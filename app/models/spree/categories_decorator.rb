Spree::Taxon.class_eval do
  scope :arrange_hot_categories_by, lambda { |category_ids|
    where(id: category_ids).sort_by { |num| category_ids.index(num.id) }
  }
end
