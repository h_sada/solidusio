Spree::Variant.class_eval do
  scope :find_by_color, lambda { |param|
    joins(:option_values).find_by("spree_option_values.presentation": param)
  }
end
