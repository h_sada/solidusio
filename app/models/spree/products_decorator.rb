Spree::Product.class_eval do
  scope :includes_master, -> { includes(master: [:default_price, :images]) }
  scope :joins_option_values, lambda { |param|
    joins(variants: :option_values).find(param)
  }
  scope :find_taxon_products, lambda { |args|
    includes_master.in_taxon(args[:taxon])
  }
  scope :find_related_products, lambda { |product|
    includes_master.in_taxons(product.taxon_ids)
  }
end
