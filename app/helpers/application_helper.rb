module ApplicationHelper
  def make_title(other_title)
    if other_title.empty?
      "PotepanEC"
    else
      other_title
    end
  end

  private

  def resource_name
    :potepan_user
  end

  def resource
    @resource ||= Spree::User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[send(:resource_name)]
  end
end
